Pour cet exercice , on souhaite afficher la bibliographie d'un auteur.

### Remarque :

Les propriétés doivent être typées pour cet exercice.
Essayer de déduire les types selon les propriétées données.

## Partie 1 :

Un auteur a un nom, un prénom, un sexe, une date de naissance et des livres à son actif.

Un livre est composé d'un titre, d'une année de parution, d'un nombre de pages et d'un prix.

Créer les classes et les attributs permettant de manipuler de tels objets.

Ajouter les getters et setters à ces deux classes (manuellement !).

- Créer un auteur et afficher ses informations
- Créer un livre et afficher ses informations

## Partie 2 :

Ajouter une méthode getAge() à la classe Auteur ainsi qu'une méthode \_\_toString() permettant d'afficher le nom, prénom et âge de l'auteur.

Ajouter une méthode \_\_toString() à la classe Livre permettant de renvoyer le titre, l'année de sortie, le nombre de pages et le prix.

- Afficher les infos des objets instanciés.

## Partie 3 :

Ajouter une propriété "auteur" à la classe livre.

Ajouter une méthode d'ajout de livres à la classe Auteur.

- Céer des livres (et les ajouter à un auteur lors de leur création).

Etapes
-> Lors de la création d'un livre :

- Ajout d'un auteur au livre.
- Ajout du livre à la liste des livres de l'auteur.

## Partie 4 :

- Afficher la bibliographie d'un auteur.
  (Methode spécifique)

## Partie 5 (Bonus):

- Donner un peu de style à votre rendu ! (CSS)

## Partie 6 (Bonus) :

- Afficher le prix total de la bibliographie dans la méthode de la partie 4.
